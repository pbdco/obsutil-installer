#!/bin/bash
#####
# One liner Obsutil install: 
# wget -qO- https://gitlab.com/pbdco/obsutil-installer/-/raw/main/obsutil_install.sh | bash -s --
#####

wget "https://obs-community-intl.obs.ap-southeast-1.myhuaweicloud.com/obsutil/current/obsutil_linux_amd64.tar.gz"
tar zxf obsutil_linux_amd64.tar.gz
cd obsutil_linux_amd64_5.3.4
chmod 755 obsutil
bash setup.sh obsutil
cp obsutil /bin/
